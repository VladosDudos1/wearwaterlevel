package vlados.dudos.waterbalance

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import vlados.dudos.waterbalance.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var waterValue = 3000
    private var startGuidelinePercentage = 1.0F

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        updateTexts()

        onClick()
    }

    private fun onClick() {
        binding.waterAddBtn.setOnClickListener {
            changeGuidelinePercentage()

        }
    }
    private fun changeGuidelinePercentage(){
        waterValue -= 250
        startGuidelinePercentage -= 0.08334F
        binding.guideline4.setGuidelinePercent(startGuidelinePercentage)

        changeViewsColors()
    }
    private fun changeViewsColors(){
        changeTextsColor()
        changeWaterViewColor()
    }
    private fun changeTextsColor(){
    }
    private fun changeWaterViewColor(){
        binding.waterView.alpha -= 0.08334F
    }
    @SuppressLint("SetTextI18n")
    private fun updateTexts(){
        binding.notDrunkYetTxt.text = "Осталось:\n$waterValue"
        binding.drunkTxt.text = "Выпито:\n" + (3000 - waterValue)
    }
}